package main

import (
	"fmt"
	"math"
	"math/cmplx"
	"math/rand"
	"os"
	"os/exec"
)

var i, j int = 1, 2

//go的基本类型
//bool string
//int int8 int16 int32 int64
//uint uint8 uint16 uint32 uint64 uintptr
//byte //==uint8
//rune //== int32== 一个unicode码
//float32 float64
//complex64 complex128
var (
	ToBe   bool       = false
	MaxInt uint64     = 1<<64 - 1
	z      complex128 = cmplx.Sqrt(-5 + 12i)
)

func add(x int, y int) int {
	return x + y
}

func swap(x, y string) (string, string) {
	return y, x
}

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

//fmt格式化字符串%v,%T的用法
type Power struct {
	age  int
	high int
	name string
}

//func main() {
//
//	var i Power = Power{age: 10, high: 178, name: "NewMan"}
//
//	fmt.Printf("type:%T\n", i)
//	fmt.Printf("value:%v\n", i)
//	fmt.Printf("value+:%+v\n", i)
//	fmt.Printf("value#:%#v\n", i)
//
//
//	fmt.Println("========interface========")
//	var interf interface{} = i
//	fmt.Printf("%v\n", interf)
//	fmt.Println(interf)
//}
func main() {
	//var c,python,java=true,false,"No!" //允许在函数外
	c, python, java := true, false, "No!" //不可以使用在函数外边
	a, b := swap("hello", "world")
	fmt.Println("Hello World哈哈哈")
	fmt.Println("RandNum is :", rand.Intn(100))
	fmt.Println("Yuo have %g problems", math.Nextafter(2, 3))
	fmt.Println(math.Pi)
	fmt.Println(add(2, 3))
	fmt.Println(a, b)
	fmt.Println(split(17))
	fmt.Println(i, j, c, python, java)
	const f = "%T(%v)\n"
	fmt.Println(f, ToBe, ToBe)
	fmt.Println(f, MaxInt, MaxInt)
	fmt.Println(f, z, z)
	cmd := exec.Command("systeminfo")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
